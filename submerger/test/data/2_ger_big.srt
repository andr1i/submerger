1
00:05:40,120 --> 00:05:42,039
Rein. In die Küche.

2
00:07:20,930 --> 00:07:21,764
Ja.

3
00:07:22,264 --> 00:07:23,140
<i>Wo sind Sie?</i>

4
00:07:24,058 --> 00:07:25,017
Keine Ahnung.

5
00:07:25,809 --> 00:07:27,019
Ich bin nur...

6
00:07:29,313 --> 00:07:31,357
nach Norden gegangen. Zwei Stunden.

7
00:07:31,982 --> 00:07:33,984
Überquerte einen Fluss. Vor einer Stunde.

8
00:07:34,068 --> 00:07:35,486
<i>Sah Sie jemand?</i>

9
00:07:35,486 --> 00:07:36,612
Nein.

10
00:07:36,612 --> 00:07:37,696
<i>Weiter so.</i>

11
00:07:38,280 --> 00:07:39,865
<i>Das ist Salamanca-Gebiet.</i>

12
00:07:39,949 --> 00:07:42,034
<i>Alle suchen nach Ihnen.</i>

13
00:07:42,034 --> 00:07:43,744
<i>Damit meine ich alle.</i>

14
00:07:43,744 --> 00:07:45,704
<i>Auch die</i> Federales.

15
00:07:49,166 --> 00:07:50,042
Ist es erledigt?

16
00:07:50,751 --> 00:07:52,252
<i>Ja. Er ist tot.</i>

17
00:07:58,926 --> 00:08:00,219
Da waren auch ein paar...

18
00:08:01,095 --> 00:08:02,388
normale Leute...

19
00:08:03,389 --> 00:08:05,057
im Haus, alte Leute.

20
00:08:06,600 --> 00:08:07,601
Und die?

21
00:08:07,685 --> 00:08:09,353
<i>Darüber weiß ich nichts.</i>

22
00:08:10,521 --> 00:08:13,148
<i>Wenn wir aufhören,
die Batterie aus dem Handy nehmen.</i>

23
00:08:13,232 --> 00:08:14,274
<i>Dann nach Norden.</i>

24
00:08:14,358 --> 00:08:15,943
<i>Außer Sicht bleiben.</i>

25
00:08:15,943 --> 00:08:18,278
<i>Ich bereite ein Versteck für Sie vor.</i>

26
00:08:18,362 --> 00:08:20,531
<i>In einer Stunde
die Batterie wieder einlegen.</i>

27
00:08:20,531 --> 00:08:22,741
<i>Ich sage Ihnen, wo Sie</i> <i>hinsollen.</i>

28
00:09:00,946 --> 00:09:02,114
Don Eduardo!

29
00:09:02,823 --> 00:09:03,741
Ist alles klar?

30
00:09:04,366 --> 00:09:06,577
Ein Unfall. Es tut mir leid.

31
00:09:07,327 --> 00:09:08,162
Darf ich?

32
00:09:08,162 --> 00:09:09,204
Natürlich!

33
00:09:14,376 --> 00:09:16,086
Zucker? Milch?

34
00:09:17,087 --> 00:09:18,630
Nein. Danke.

35
00:09:35,939 --> 00:09:36,857
Köstlich!

36
00:09:39,109 --> 00:09:40,069
Was ist los?

